" Vundle {{{
" === Vundle ===
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'chriskempson/base16-vim'
Plugin 'mattn/emmet-vim'
Plugin 'myusuf3/numbers.vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'raimondi/delimitmate'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'scrooloose/syntastic'
Plugin 'pangloss/vim-javascript'
Plugin 'elzr/vim-json'
Plugin 'hail2u/vim-css3-syntax'
Plugin 'amirh/HTML-AutoCloseTag'
Plugin 'tpope/vim-vinegar'
Plugin 'tpope/vim-surround'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

call vundle#end()            " required
filetype plugin indent on    " required
" }}}
" Standard Vim settings {{{
syntax enable		      " enable syntax processing
set encoding=utf-8
set hidden
set ttyfast
set number                " show line numbers
set showmatch		      " show matching brackets
set cursorline
set wildmenu		      " show autocomplete suggestions for command menu
set shiftwidth=2          " tab settings for auto-indent
set tabstop=2		      " number of visual spaces per tab
set expandtab		      " tabs are spaces
set autoindent
" }}}
" Folding {{{
"=== folding ===
set foldmethod=marker
set foldlevel=0
set foldnestmax=10
set foldenable
set modelines=1
" }}}
" Syntastic {{{
let g:syntastic_javascript_checkers = ["eslint"]

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_html_tidy_ignore_errors = ['proprietary attribute']
let g:syntastic_css_csslint_ignore_errors = ['Unknown property']
" }}}
" UI Customization {{{
let g:airline_powerline_fonts = 1
let g:airline_theme = 'base16color'
let g:airline_skip_empty_sections = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#syntastic#enabled = 1
let g:airline_symbols = get(g:,'airline_symbols',{})
let g:airline_symbols.maxlinenr=''
let g:ctrlp_working_path_mode = ''
let g:ctrlp_custom_ignore = {
  \ 'dir':  'node_modules',
  \ 'file': '\v\.(exe|so|dll)$',
  \ }
set laststatus=2
set background=dark
let base16colorspace=256
colorscheme base16-brewer

hi MatchParen cterm=none ctermbg=green ctermfg=white
hi Normal ctermfg=15 ctermbg=none
hi LineNr ctermfg=8 ctermbg=none
hi CursorLine ctermbg=black
hi CursorLineNR ctermbg=green
" }}}
" Keybinding Customization {{{
let mapleader=","       " set leader to comma
set backspace=indent,eol,start
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')   "move within wrap if used without count
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')   "move within wrap if used without count
nnoremap tl :bnext<CR>
nnoremap th :bprevious<CR>
" }}}
" Backup Buffers in /tmp {{{
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup
" }}}
" Emmet {{{
let g:user_emmet_install_global = 0
autocmd FileType html,css,php EmmetInstall
let g:user_emmet_leader_key=','
" }}}
" vim:foldmethod=marker:foldlevel=0
