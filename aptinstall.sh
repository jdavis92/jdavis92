#!/bin/sh
LIST_OF_APPS="git vlc browser-plugin-vlc livestreamer gimp gimp-data gimp-plugin-registry gimp-data-extras libav-tools imagemagick xfce4-terminal vim nodejs python-dev python-pip virtualbox vagrant xinit i3 conky numix-gtk-theme gtk-chtheme feh docker-engine"

NODEJS_TOOLS="gulp bower"

curl -sL https://deb.nodesource.com/setup_6.x | bash -

add-apt-repository -y ppa:otto-kesselgulasch/gimp #add gimp repo
add-apt-repository -y ppa:numix/ppa
add-apt-repository -y ppa:git-core/ppa
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo deb https://apt.dockerproject.org/repo ubuntu-xenial main >> /etc/apt/sources.list.d/docker.list
apt-get update
apt-get install -y $LIST_OF_APPS
npm install -g npm@latest && npm install -g $NODEJS_TOOLS
pip install ansible --upgrade
vagrant plugin install vagrant-bindfs vagrant-hostmanager

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
git clone https://github.com/sunaku/tamzen-font/ ~/.fonts/tamzen-font
xset +fp ~/.fonts/tamzen-font/bdf
xset fp rehash

cd /tmp
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb

git clone https://jdavis92@bitbucket.org/jdavis92/jdavis92.git
mv jdavis92/* ~/
mv ~/bin/* /usr/local/bin/

echo "Cleaning Up" &&
sudo apt-get -f install &&
sudo apt-get autoremove &&
sudo apt-get -y autoclean &&
sudo apt-get -y clean
